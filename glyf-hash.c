#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>

#include <getopt.h>

#include <ft2build.h>
#include FT_FREETYPE_H
#include FT_TRUETYPE_TABLES_H
#include FT_TRUETYPE_TAGS_H

/* libcrc */
#include "checksum.h"

/* librhash */
#include "rhash.h"

/* libxxhash */
#include "xxhash.h"

/* ---- global config variables ----- */
FT_Bool  debug = 0;
FT_Bool  debugMemory = 0;
FT_Bool  showNullPath = 0;
FT_Bool  dumpSvgDesc = 0;
FT_Bool  showZeroGlyph = 0;
int      faceIndex = 0;

char* optstring = "df:mnsz";
struct option longopts[] = {
  { "debug",          no_argument, NULL, 'd' },
  { "face",           required_argument, NULL, 'f' },
  { "debug-memory",   no_argument, NULL, 'm' },
  { "show-null-path", no_argument, NULL, 'n' },
  { "show-svg",       no_argument, NULL, 's' },
  { "show-zero-glyph",no_argument, NULL, 'z' },
  { NULL,             0,           NULL,  0  }
};

/* ---------------------------------- */
void parseArg(int argc, char** argv)
{
  while (1) {
    int opt, option_index = 0;
    if (0 > (opt = getopt_long(argc, argv, optstring, longopts, &option_index)))
      break;

    switch (opt) {
    /* options with no argument */
    case 'd': debug = 1;        break;
    case 'm': debugMemory = 1;  break;
    case 'n': showNullPath = 1; break;
    case 's': dumpSvgDesc = 1;  break;
    case 'z': showZeroGlyph = 1;break;

    /* options with 1 required argument */
    case 'f':
      faceIndex = (int)strtoul(optarg, NULL, 0);
      break;
    }
  }
}

void printHelp(char* prog_name)
{
  printf("%s: <font_file_pathname>\n", prog_name);
  exit(1);
}

FT_Bool ft_vector_eq(const FT_Vector* a, const FT_Vector* b)
{
  if (a->x == b->x && a->y == b->y)
    return 1;
  else
    return 0;
}

void printTtfTime(uint32_t ttfTime0, uint32_t ttfTime1)
{
  char    buf[11];
  time_t  time  = (time_t)ttfTime1;

  /* ignore most of upper bits until 2176 and adjust epoch */
  time  = (ttfTime0 == 1) ? (time  + 2212122496U)
                          : (time  - 2082844800U);

  /* assume newer than 1970, to use strftime()  */
  if ( time >= 0 )
  {
    strftime( buf, sizeof ( buf ), "%Y-%m-%d", gmtime( &time ) );
    printf( "%s", buf );
  } else {
    printf( "(older than 1970)" );
  }
}

unsigned long getNthOffsetFromLoca( unsigned long   n,
                                    size_t          loca_entry_len,
                                    unsigned long   loca_len,
                                    unsigned char*  loca_buff )
{
  unsigned long  i, j, off;


  if (loca_entry_len != 2 && loca_entry_len != 4)
    return 0;

  i = n * loca_entry_len;
  j = i + loca_entry_len;

  off = 0;
  if (i < loca_len && j <= loca_len) {
    for (;i < j; i ++) {
      off = (off << 8) + loca_buff[i];
    }
  }

  if (loca_entry_len == 2) /* loca format == 0 */
    off = off * 2;
  return off;
}

void dumpMD5(unsigned long len, const unsigned char* buff)
{
  int res;
  unsigned char digest[64];
  char digest_hexstr[130];


  rhash_library_init();
  res = rhash_msg(RHASH_MD5, buff, len, digest);
  if (res < 0)
    printf(" md5=N/A");
  else {
    rhash_print_bytes(digest_hexstr, digest,
                      rhash_get_digest_size(RHASH_MD5),
                      (RHPR_HEX | RHPR_UPPERCASE) );
    printf(" md5=%s", digest_hexstr);
  }
}

void dumpHash(unsigned long len, const unsigned char* buff)
{
  uint8_t  csum_crc8 = crc_8( buff, len );
  printf(" crc8=%02X", csum_crc8);

  uint16_t  csum_crc16 = crc_16( buff, len );
  printf(" crc16=%04X", csum_crc16);

  uint32_t  csum_crc32 = crc_32( buff, len );
  printf(" crc32=%08X", csum_crc32);

  uint64_t  csum_crc64 = crc_64_ecma( buff, len );
  printf(" crc64=%016lX", csum_crc64);

  dumpMD5(len, buff);

  XXH32_hash_t hs_xxh32 = XXH32( buff, len, 0 );
  printf(" xxh32=%08X", hs_xxh32);

  XXH64_hash_t hs_xxh64 = XXH64( buff, len, 0 );
  printf(" xxh64=%016lX", hs_xxh64);

  XXH128_hash_t hs_xxh128 = XXH3_128bits_withSeed( buff, len, 0 );
  printf(" xxh128=%016lX%016lX", hs_xxh128.high64, hs_xxh128.low64);
}

void dumpHexStr(size_t len, char* buff)
{
  unsigned long i;
  printf(" <");
  for (i = 0; i < len; i++ ) {
    /* printf(" 0x"); */
    printf("%02X", buff[i]);
  }
  printf(">");
}

typedef struct {
  char*  buff;
  long   pos;
  long   size;
  long   last_M;
  long   last_CLQ;

  int       numInvalidOps;
  FT_Bool   debugEmitter;
  FT_Bool   includeNullPath;
  FT_Vector cur;
} svg_d;

int svg_d_extend(svg_d* svg_buff, char* str)
{
  size_t size_total = svg_buff->size;
  size_t size_left  = svg_buff->size - svg_buff->pos;
  size_t size_new = 0;
  size_t len_cur  = svg_buff->pos;
  size_t len_next = strlen(str);

  if (len_next < size_left)
    size_new = 0; /* no need to reallocate */
  else if (size_total < len_next)
    size_new = size_total + len_next * 2;
  else
    size_new = size_total * 2;

  if (debugMemory)
    fprintf(stderr, "  buff (total=%d, used=%d, left=%d) + token(%d)",
                             size_total, len_cur, size_left, len_next);
  if (size_new) {
    if (debugMemory)
      fprintf(stderr, " -> buff2(total=%d, left=%d)",
                           size_new, size_new - len_cur - len_next );
    svg_buff->size = size_new;
    svg_buff->buff = (char*)realloc(svg_buff->buff, svg_buff->size);
  }
  if (debugMemory)
    fprintf(stderr, "\n");
  strncat(svg_buff->buff, str, svg_buff->size);
  svg_buff->pos = strlen(svg_buff->buff);
  return svg_buff->pos;
}

/*
 * check whether the final line consists only one M operator
 */
int svg_d_rm_trailing_M(svg_d* svg_buff)
{
  char* c = NULL;
  char  trailing_M[128]; /* M[0-9]+,[0-9]+ */

  /* no last M op, nothing to remove */
  if (svg_buff->last_M < 0)
    return 0;

  /* if any C/L/Q op exists after the last M op,
   * nothing to remove.
   */
  if (0 < svg_buff->last_CLQ && svg_buff->last_M < svg_buff->last_CLQ)
    return 0;

  svg_buff->numInvalidOps += 1;
  c = svg_buff->buff + svg_buff->last_M;

  if (svg_buff->debugEmitter) {
    printf("#\t# invalidate strayed M operator: %s\n", c);
    strncpy(trailing_M, c, sizeof(trailing_M));
    *c = '\0'; /* delete the last M op */
    svg_d_extend(svg_buff, "X");
    svg_d_extend(svg_buff, trailing_M);
  } else {
    *c = '\0'; /* delete the last M op */

    /* the 2nd or later M op is after '\n' */
    if (svg_buff->buff < c && c[-1] == '\n') {
      c--;
      *c = '\0';
    }
  }

  /* backtrack to previous M */
  for (; svg_buff->buff <= c; c--) {
    if (*c != 'M')
      continue;

    svg_buff->last_M = c - svg_buff->buff;
    break;
  }

  return 1;
}

int outline_emitter_moveto(const FT_Vector* to, void* user)
{
  svg_d* svg_buff = (svg_d*)user;

  char   buff[1024];
  snprintf(buff, sizeof(buff), "M%d,%d", to->x, to->y);

  if (0 < svg_buff->pos) {
    svg_d_rm_trailing_M(svg_buff);
    svg_d_extend(svg_buff, "\n");
  }
  svg_buff->last_M = svg_buff->pos;
  svg_d_extend(svg_buff, buff);
  svg_buff->cur = *to;

  return 0;
}

int outline_emitter_lineto(const FT_Vector* to, void* user)
{
  svg_d* svg_buff = (svg_d*)user;

  char   buff[1024];
  if (svg_buff->cur.x == to->x && svg_buff->cur.y == to->y)
  {
    svg_buff->numInvalidOps += 1;
    if (svg_buff->debugEmitter)
      printf("#\t# invalidate zero-length operator: L%d,%d\n", to->x, to->y);
    if (!svg_buff->includeNullPath)
      return 0;
    snprintf(buff, sizeof(buff), " XL%d,%d", to->x, to->y);
  } else {
    snprintf(buff, sizeof(buff), " L%d,%d", to->x, to->y);
    svg_buff->last_CLQ = svg_buff->pos + 1; /* +1 is for delimiter space */
  }
  svg_d_extend(svg_buff, buff);
  svg_buff->cur = *to;

  return 0;
}

int outline_emitter_conicto(const FT_Vector* ctrl, const FT_Vector* to, void* user)
{
  svg_d* svg_buff = (svg_d*)user;

  char   buff[1024];
  if (ft_vector_eq(&(svg_buff->cur), to) && ft_vector_eq(ctrl, to)) {
    svg_buff->numInvalidOps += 1;
    if (svg_buff->debugEmitter)
      printf("#\t# invalidate zero-length operator: Q%d,%d,%d,%d\n",
                                              ctrl->x, ctrl->y, to->x, to->y);
    if (!svg_buff->includeNullPath)
      return 0;
    snprintf(buff, sizeof(buff), " XQ%d,%d,%d,%d",
                                 ctrl->x, ctrl->y,
                                 to->x, to->y);
  } else {
    snprintf(buff, sizeof(buff), " Q%d,%d,%d,%d",
                                 ctrl->x, ctrl->y,
                                 to->x, to->y);
    svg_buff->last_CLQ = svg_buff->pos + 1; /* +1 is for delimiter space */
  }
  svg_d_extend(svg_buff, buff);
  svg_buff->cur = *to;

  return 0;
}

int outline_emitter_cubicto(const FT_Vector* ctrl1, const FT_Vector* ctrl2,
                            const FT_Vector* to, void* user)
{
  svg_d* svg_buff = (svg_d*)user;
  char   buff[1024];

  if (ft_vector_eq(ctrl1, to) && ft_vector_eq(ctrl2, to) &&
      ft_vector_eq(&(svg_buff->cur), to))
  {
    svg_buff->numInvalidOps += 1;
    if (svg_buff->debugEmitter)
      printf("#\t# invalidate zero-length operator: C%d,%d,%d,%d\n",
                                              ctrl1->x, ctrl1->y,
                                              ctrl2->x, ctrl2->y, to->x, to->y);
    if (!svg_buff->includeNullPath)
      return 0;
    snprintf(buff, sizeof(buff), " XC%d,%d,%d,%d,%d,%d",
                                  ctrl1->x, ctrl1->y,
                                  ctrl2->x, ctrl2->y,
                                  to->x, to->y);
  } else {
    snprintf(buff, sizeof(buff), " C%d,%d,%d,%d,%d,%d",
                                  ctrl1->x, ctrl1->y,
                                  ctrl2->x, ctrl2->y,
                                  to->x, to->y);
    svg_buff->last_CLQ = svg_buff->pos + 1; /* +1 is for delimiter space */
  }
  svg_d_extend(svg_buff, buff);
  svg_buff->cur = *to;

  return 0;
}

char* getSvgFromGid(FT_Face ft2face, FT_UInt gid,
                    FT_Bool debugEmitter,
                    FT_Bool includeNullPath,
                    int*    numInvalidOps)
{
  FT_Error          ft2err;
  FT_Outline_Funcs  svg_emitters;
  svg_d             svg_buff;

  svg_emitters.move_to  = outline_emitter_moveto;
  svg_emitters.line_to  = outline_emitter_lineto;
  svg_emitters.conic_to = outline_emitter_conicto;
  svg_emitters.cubic_to = outline_emitter_cubicto;
  svg_emitters.shift = 0;
  svg_emitters.delta = 0;

  svg_buff.buff = (char*)malloc(256);
  svg_buff.buff[0] = '\0';
  svg_buff.pos = 0;
  svg_buff.size = 256;
  svg_buff.last_M   = -1;
  svg_buff.last_CLQ = -1;
  svg_buff.numInvalidOps = 0;
  svg_buff.debugEmitter = debugEmitter;
  svg_buff.includeNullPath = includeNullPath;


  if (debug)
    fprintf(stderr, "get SVG for gid=%d\n", gid);
  ft2err = FT_Load_Glyph(ft2face, gid, 0 | FT_LOAD_NO_SCALE
                                         | FT_LOAD_VERTICAL_LAYOUT
                                         | FT_LOAD_NO_HINTING
                                         | FT_LOAD_NO_AUTOHINT );
  ft2err = FT_Outline_Decompose(&(ft2face->glyph->outline), &svg_emitters, &svg_buff);
  // FT_Done_Glyph(ft2face->glyph);
  svg_d_rm_trailing_M(&svg_buff);
  svg_d_extend(&svg_buff, "\n");

  if (numInvalidOps)
    *numInvalidOps = svg_buff.numInvalidOps;

  return svg_buff.buff;
}

void printLinesWithPrefix(const char* prefix, const char* lines)
{
  size_t len = strlen(lines);
  char *cur, *eol, *buff;
  buff = (char*)malloc(len + 1);
  strncpy(buff, lines, len);
  for (cur = buff; cur < buff + len; cur = eol + 1) {
    eol = index(cur, '\n');
    if (eol < buff + len)
      *eol = '\0';
    printf(prefix);
    printf(cur);
    puts("");
  }
  free(buff);
}

void dumpGidSvg(FT_Face ft2face, FT_ULong* ucss, FT_UShort gid)
{
  /* for hashing, the first loading of SVG ops should be silent, exclude null path */
  FT_Error ft2err;
  char   gname[256];
  int    numInvalidOps = 0;
  char*  svg_str = getSvgFromGid(ft2face, gid, 0, 0, &numInvalidOps);
  size_t svg_len = strlen(svg_str);

  /* svg_str is terminated by '\n' */
  if (!showZeroGlyph && svg_len < 2)
    return;

  if (ucss) {
    FT_ULong* ucs;
    printf("ucs=");
    for (ucs = ucss; *ucs; ucs++) {
      if (ucss < ucs)
        printf("_");
      printf("U+%04X", *ucs);
    }
    printf(" ");
  }

  if (FT_Err_Ok == FT_Get_Glyph_Name(ft2face, gid, gname, 255))
    printf("name=%s ", gname);
 
  printf("gid=%d ", gid);

  printf("svg_len=%d ", svg_len);
  printf("invalid_ops=%d", numInvalidOps);
  dumpHash(svg_len, svg_str);
  printf("\n");

  if (!showNullPath) {
    if (dumpSvgDesc)
      printLinesWithPrefix("#\t", svg_str);
  }
  free(svg_str);

  if (dumpSvgDesc && showNullPath) {
    svg_str = getSvgFromGid(ft2face, gid, dumpSvgDesc, 1, NULL);
    printLinesWithPrefix("#\t", svg_str);
    free(svg_str);
  }
}

int main(int argc, char** argv)
{
  FT_Error    ft2err;
  FT_Library  ft2lib;
  FT_Face     ft2face;
  TT_Header*      tt_head;
  int             i, j, opt;
  FT_UShort       gid;


  parseArg(argc, argv);
  if (optind >= argc) {
    printHelp(argv[0]);
  }

  ft2err = FT_Init_FreeType( &ft2lib );
  if (ft2err != FT_Err_Ok) {
    printf("*** Could not initialize FreeType2 library\n");
    return -1;
  }

  ft2err = FT_New_Face(ft2lib, argv[optind], faceIndex, &ft2face);
  if (ft2err != FT_Err_Ok) {
    printf("*** Could not open the face #%d from %s\n", faceIndex, argv[optind]);
    return -1;
  }

  {
    char* f = rindex(argv[optind], '/');
    if (f)
      f += 1;
    else
      f = argv[optind];
    printf("# file: %s\n", f);
    printf("# face: %d\n", faceIndex);
    printf("# family: %s\n", ft2face->family_name ? ft2face->family_name : "(null)");
    printf("# style: %s\n", ft2face->style_name ? ft2face->style_name : "(null)");
  }

  if (FT_IS_SFNT(ft2face)) {
    tt_head = FT_Get_Sfnt_Table( ft2face, ft_sfnt_head );

    if (tt_head->Font_Revision & 0xFFC0) 
      printf("# head.fontRevision: %.4g\n", tt_head->Font_Revision / 65536.0 );
    else
      printf("# head.fontRevision: %.2f\n", tt_head->Font_Revision / 65536.0 );

    printf("# num_glyphs: %d\n", ft2face->num_glyphs);

    /* OpenType spec, since 1.8.4, declare LONGDATETIME as a signed 64-bit int */
    /* but older FreeType (< 2.9.1) loaded it as two signed 32-bit ints, thus */
    /* following does not work well */
    printf("# head.Created: ");
    printTtfTime((uint32_t)tt_head->Created[0], (uint32_t)tt_head->Created[1]);
    puts("");

    printf("# head.Modified: ");
    printTtfTime((uint32_t)tt_head->Modified[0], (uint32_t)tt_head->Modified[1]);
    puts("");
  }

  if (ft2face->charmap) {
    FT_UInt   uiGid = FT_UINT_MAX;
    FT_ULong  ulCode = FT_Get_First_Char(ft2face, &uiGid);

    for (ulCode = FT_Get_First_Char(ft2face, &uiGid);
         uiGid < FT_USHORT_MAX && 0 < uiGid && uiGid < ft2face->num_glyphs;
         ulCode = FT_Get_Next_Char(ft2face, ulCode, &uiGid))
    {
      FT_UInt32* uvss;
      FT_ULong ucss[3] = {ulCode, 0, 0};
      gid = (FT_UShort)uiGid;

      dumpGidSvg(ft2face, ucss, gid);

      uvss = FT_Face_GetVariantsOfChar(ft2face, ulCode);
      if (uvss) {
        int i = 0;
        for (i = 0; uvss[i]; i ++) {
          ucss[1] = uvss[i];
          gid = FT_Face_GetCharVariantIndex(ft2face, ulCode, uvss[i]);
          if (!gid)
            continue;
          dumpGidSvg(ft2face, ucss, gid);
        }
      }
    }
  } else {
    for (gid = 0; gid < ft2face->num_glyphs && gid < FT_USHORT_MAX; gid++)
      dumpGidSvg(ft2face, NULL, gid);
  }
  FT_Done_Face(ft2face);
  FT_Done_FreeType(ft2lib);
}
