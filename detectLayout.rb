#!/usr/bin/env ruby1.9
Encoding.default_internal = "utf-8"
Encoding.default_external = "utf-8"
require "utils.rb"
require "json"
require "nokogiri"
require "nokogiri-class.rb"
require "libRect.rb"
Opts = {
  "dump-json" => false,

  "fname-ucshex" => "Arial",
  "fsize-ucshex" => 9.72653,
  # "fsize-hanzi" => 20.4261,
  "fsize-hanzi" => 18,
  "fname-gname" => "UCSArialNarrow",
  "fsize-gname" => 5.83604
}
require "getOpts.rb"

def sortZRects(rects)
  rows = Array.new()
  rects.sort_by{|r| r.avgY}.each do |a|
    if (rows.length > 0 && rows.last.hasOverlapYWith(a))
      rows.last.extendToCover(a)
      rows.last.data["member"] << a
    else
      rows << Rect.new().setByX1Y1X2Y2(a.x1, a.y1, a.x2, a.y2)
      rows.last.data = Hash.new()
      rows.last.data["member"] = [ a ]
    end
  end

  o = Array.new()
  rows.each do |r|
    r.data["member"].sort_by{|a| a.x1}.each do |a|
      o << a
    end
  end

  return o
end



class Page
  attr_accessor(:pageNumber)
  attr_accessor(:arrUcshex, :arrHanzi, :arrGname, :arrRS)
  attr_accessor(:firstUcs, :lastUcs)

  def initialize()
    @arrUcshex = Array.new()
    @arrHanzi = Array.new()
    @arrGname = Array.new()
    @arrRS = Array.new()
  end

  def getBBox()
    r = Rect.new()
    (@arrUcshex + @arrHanzi + @arrGname + @arrRS).each_with_index do |a, i|
      if (i == 0)
        r.setByX1Y1X2Y2(a.x1, a.y1, a.x2, a.y2)
      else
        r.extendToCover(a)
      end
    end
    return r
  end

  def getUcshexGrouped()
    grpRect = Array.new()
    @arrUcshex.each do |aRect|
      oldGrp = grpRect.select{|grpRect| grpRect.includesX(aRect.avgX)}.first
      if (oldGrp)
        oldGrp.extendToCover(aRect)
        oldGrp.data["member-ucshex"] << aRect
      else
        newGrp = Rect.new().setByX1Y1X2Y2( aRect.x1, aRect.y1, aRect.x2, aRect.y2 )
        newGrp.data = Hash.new()
        newGrp.data["member-ucshex"] = [ aRect ]
        grpRect << newGrp
      end
    end

    grpRect = grpRect.sort_by{|aRect| aRect.x1}
    grpRect.each do |aGrp|
      aGrp.data["member-ucshex"] = aGrp.data["member-ucshex"].sort_by{|aRect| aRect.y1}
    end

    @arrGname.each do |aRect|
      next if (grpRect.none?{|aGrp| aGrp.hasOverlapXWith(aRect)})
      @arrRS << aRect
    end
    @arrGname -= @arrRS

    return grpRect
  end

  def getGroup()
    grpRect = self.getUcshexGrouped()

    grpRect.each_cons(2) do |grp1, grp2|
      xRange = grp1.x1..grp2.x1
      gnames = self.arrGname.select{|aRect| xRange.include?(aRect.avgX)}
      hanzis = self.arrHanzi.select{|aRect| xRange.include?(aRect.avgX)}
      gnames.each{|aRect| grp1.extendToCover(aRect)}
      hanzis.each{|aRect| grp1.extendToCover(aRect)}
      grp1.data["member-gname"] = gnames
      grp1.data["member-hanzi"] = hanzis
    end

    grp1 = grpRect.last
    grp1.data["member-gname"] = self.arrGname.select{|aRect| grp1.x1 < aRect.x1}
    grp1.data["member-gname"].each{|aRect| grp1.extendToCover(aRect)}
    grp1.data["member-hanzi"] = self.arrHanzi.select{|aRect| grp1.x1 < aRect.x1}
    grp1.data["member-hanzi"].each{|aRect| grp1.extendToCover(aRect)}

    return grpRect
  end

  def getBoundsPerUcshex()
    grpRect = self.getGroup()

    pageBBox = self.getBBox()

    grpRect.each do |grp0|
      grp0.data["codes"] = Array.new()

      rectUcshex = grp0.data["member-ucshex"].sort_by{|a| a.y1}
      rectUcshex << Rect.new().setByX1Y1X2Y2( rectUcshex.last.x1,
                                              pageBBox.y2,
                                              rectUcshex.last.x2,
                                              pageBBox.y2)
      rectUcshex.each_cons(2) do |uh1, uh2|
        uRect = Rect.new().setByX1Y1X2Y2(uh1.x1, uh1.y1, uh1.x2, uh2.y2)
        grp0.data["codes"] << uRect
        uRect.data = Hash.new
        uRect.data["ucshex"] = uh1
        uRect.data["member-gname"] = Array.new()
        uRect.data["member-hanzi"] = Array.new()
        yRange = uh1.y1..uh2.y1
        grp0.data["member-gname"].select{|a| yRange.include?(a.avgY)}.each do |a|
          uRect.extendToCover(a)
          uRect.data["member-gname"] << a
        end
        grp0.data["member-hanzi"].select{|a| yRange.include?(a.avgY)}.each do |a|
          uRect.extendToCover(a)
          uRect.data["member-hanzi"] << a
        end
      end
    end

    return grpRect
  end

  def mapHanziAndGname()
    grpRect = self.getBoundsPerUcshex()
    grpRect.each do |aGrp|
      aGrp.data["codes"].each do |aCode|
        aCode.data["member-hanzi"].each do |ahz1|
          # printf("lookup gnames for gid=%d\n", ahz1.data["gid"])

          gnames = aCode.data["member-gname"].
                     select{|a| ahz1.hasOverlapXWith(a) && ahz1.y1 < a.y1}.
                     sort_by{|a| a.y1}

          # printf("  candidates: %s\n", gnames.map{|a| a.data["ustring"]}.join(", "))

          ahz2 = aCode.data["member-hanzi"].
                   select{|a| ahz1 != a && ahz1.hasOverlapXWith(a) && ahz1.y1 < a.y1}.
                   sort_by{|a| a.y1}.first
          if (ahz2)
            # printf("    hanzi under current one: %d\n", ahz2.data["gid"])
            gnames = gnames.select{|a| a.y2 < ahz2.y1}
          end

          # printf("  selected: %s\n", gnames.map{|a| a.data["ustring"]}.join(", "))
          ahz1.data["gname"] = gnames
        end
      end
    end
    return grpRect
  end
end

def procXml(fpath, ojs)
  fh = File::open(fpath)
  xml = Nokogiri::XML::Document.parse(fh.read())
  fh.close()

  fontInfos = Array.new()
  xml.css("font-cache > font").each do |elmFONT|
    iFontRef = elmFONT["font-ref"].to_i()
    fontInfos[iFontRef] = elmFONT
  end

  xml.css("page").each do |elmPAGE|
    aPage = Page.new()  
    aPage.pageNumber = elmPAGE["page-number"].to_i()
    elmPAGE.css("text-list > text-box").each do |elmTB|
      aRect = Rect.new().setByXYWH( elmTB["x"].to_f(), elmTB["y"].to_f(),
                                    elmTB["width"].to_f(), elmTB["height"].to_f() )
      aRect.data = Hash.new()
      aRect.data["font-ref"] = elmTB["font-ref"].to_f()
      aRect.data["font-size"] = elmTB["font-size"].to_f()
      iFontRef = elmTB["font-ref"].to_i()
      fFontSize = elmTB["font-size"].to_f()
      aRect.data["font-name"] = fontInfos[iFontRef]["name"]

      aRect.data["ustring"] = elmTB.css("ustring").first.text

      sFontName = aRect.data["font-name"].split("+", 2).last

      if (fFontSize == Opts.fsize_ucshex && sFontName == Opts.fname_ucshex)
        # avoid "HEX", "C", "J", "K", "V" in URO chart header
        if (aRect.data["ustring"] =~ /^[0-9A-F]{4,5}$/)
          aPage.arrUcshex << aRect
        end
      elsif (fFontSize == Opts.fsize_gname && sFontName == Opts.fname_gname)
        aPage.arrGname << aRect
      elsif (Opts.fsize_hanzi < fFontSize && elmTB.css("glyph").length == 1)
        aRect.data["gid"] = elmTB.css("glyph").first["gid"].to_i()
        aPage.arrHanzi << aRect
      end
    end
    # puts aPage.arrUcshex.map{|aRect| aRect.data["ustring"]}.join(", ")

    # aPage.getUcshexGrouped().each_with_index do |aRect, i|
    #   printf("page=%d, group=%d, member=%s\n",
    #          aPage.pageNumber, i,
    #          aRect.data["member-ucshex"].map{|am| am.data["ustring"] }.join(", ")
    #   )
    # end

    # aPage.getGroup().each_with_index do |aRect, i|
    #   printf("page=%d, group=%d, member=%s\n",
    #          aPage.pageNumber, i,
    #          # aRect.data["member-ucshex"].map{|am| am.data["ustring"] }.join(", ")
    #          aRect.data["member-gname"].map{|am| am.data["ustring"] }.join(", ")
    #   )
    # end

    # aPage.getBoundsPerUcshex().each_with_index do |aGrp, i|
    aPage.mapHanziAndGname().each_with_index do |aGrp, i|
      aGrp.data["codes"].each_with_index do |aCode, j|
        printf("page=%d, group=%d, codes=%d, ucs=%s, gids=%s, member=%s\n",
               aPage.pageNumber, i, j,
               aCode.data["ucshex"].data["ustring"],

               sortZRects( aCode.data["member-hanzi"] ).
                 map{|a| a.data["gid"].to_s()}.join("//"),

               # aCode.data["member-gname"].
               #   map{|a| a.data["ustring"]}.join("/")

               # sortZRects( aCode.data["member-gname"] ).
               #   map{|a| a.data["ustring"]}.join("/")

               sortZRects( aCode.data["member-hanzi"] ).map{|a|
                 a.data["gname"].map{|b| b.data["ustring"]}.join("/")
               }.join("//")
        ) if (!Opts.dump_json)
        aCode.data["member-hanzi"].each do |ahz|
          hs = Hash.new
          hs["ucs"] = "U+" + aCode.data["ucshex"].data["ustring"]
          hs["glyphname"] = ahz.data["gname"].map{|b| b.data["ustring"]}.join("")
          hs["fontname"] = ahz.data["font-name"]
          hs["gid"] = ahz.data["gid"]
          hs["page"] = aPage.pageNumber
          hs["column"] = i + 1
          ojs << hs
        end
      end
    end
  end
end


ojs = Array.new()
Opts.args.each{|fp| procXml(fp, ojs)}
puts ojs.to_lined_json if (Opts.dump_json)
