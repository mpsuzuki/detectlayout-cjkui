VPATH = .:libcrc/lib:RHash/librhash:xxHash
CFLAGS = -g3 -ggdb -O0 -fkeep-inline-functions
all: glyf-hash.exe ft2svg.exe

glyf-hash.exe: glyf-hash.c libcrc.a librhash.a libxxhash.a
	$(CC) $(CFLAGS) -o $@ \
		`pkg-config --cflags freetype2` \
		-Ilibcrc/include -Llibcrc/lib \
		-IRHash/librhash -LRHash/librhash \
		-IxxHash -LxxHash \
		glyf-hash.c \
		`pkg-config --libs freetype2` \
		-lcrc -lrhash -lxxhash

libcrc.a:
	make -C libcrc

librhash.a:
	(cd RHash && ./configure --prefix=root --enable-lib-static --disable-lib-shared --disable-openssl)
	make -C RHash/librhash librhash.a

libxxhash.a:
	make -C xxHash libxxhash.a

ft2svg.exe: ft2svg.c
	$(CC) $(CFLAGS) -o $@ $^ \
		`pkg-config --cflags freetype2` \
		`pkg-config --libs freetype2` \
		-lm
