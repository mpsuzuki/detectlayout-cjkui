#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>

#include <ft2build.h>
#include FT_FREETYPE_H
#include FT_OUTLINE_H
#include FT_BBOX_H

#define SCALE_F 1.0

// Global parameters
FT_Bool  useSvgFontAndText = 0;
FT_Bool  doFill = 0;
FT_Bool  scanAllGlyphs = 0;
FT_Bool  useHint = 0;
FT_Bool  useAutoHint = 0;
FT_Bool  keepBearingX = 0;
FT_ULong firstValidGid = FT_ULONG_MAX;
FT_Int   faceIndex = 0;
FT_Bool  addUcsInfo = 0;

char*     glyphNameToSetGID = NULL;
FT_ULong  codeToSetGID = 0;



void printSvgHeader(FT_Glyph_Metrics*  metrics)
{
  printf("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n");
  printf("<svg");
  printf(" xmlns=\"http://www.w3.org/2000/svg\"");
  printf(" xmlns:xlink=\"http://www.w3.org/1999/xlink\"");
  if (metrics)
    printf(" viewBox=\"0 0 %d %d\"", metrics->horiAdvance, metrics->height);
  printf(">\n");
}

void printHtmlAndSvgHeader()
{
  printf("<html>\n");
  printf("<body>\n");
  printf("<svg ");
  printf(" xmlns=\"http://www.w3.org/2000/svg\"");
  printf(" xmlns:xlink=\"http://www.w3.org/1999/xlink\"");
  printf(" style=\"display: none;\">\n");
}

void printFaceMetrics(FT_Face  ft2face)
{
  
  printf("units-per-em=\"%d\"", ft2face->units_per_EM);
  if (!useSvgFontAndText)
    printf(" ascent=\"%d\" descent=\"%d\"", ft2face->ascender, - ft2face->descender);
}

void printGlyphMetricHorizontal(FT_Glyph_Metrics  metrics)
{
  if (ceil(metrics.horiBearingX) == metrics.horiBearingX && ceil(metrics.horiAdvance) == metrics.horiAdvance) {
    printf("horiz-origin-x=\"%d\" horiz-adv-x=\"%d\"", (int)ceil(metrics.horiBearingX / SCALE_F), (int)ceil(metrics.horiAdvance / SCALE_F));
  } else {
    printf("horiz-origin-x=\"%f\" horiz-adv-x=\"%f\"", metrics.horiBearingX / SCALE_F, metrics.horiAdvance / SCALE_F);
  }
}

void printGlyphMetricVertical(FT_Glyph_Metrics  metrics)
{
  if (ceil(metrics.vertBearingX) == metrics.vertBearingX &&
      ceil(metrics.vertBearingY) == metrics.vertBearingY && ceil(metrics.vertAdvance) == metrics.vertAdvance) {
    printf("vert-origin-x=\"%d\" vert-origin-y=\"%d\" vert-adv-y=\"%d\"",
            (int)ceil((metrics.horiBearingX - metrics.vertBearingX) / SCALE_F),
            (int)ceil((metrics.horiBearingY + metrics.vertBearingY) / SCALE_F),
            (int)ceil(metrics.vertAdvance / SCALE_F)
    );
  } else {
    printf("vert-origin-x=\"%f\" vert-origin-y=\"%f\" vert-adv-y=\"%f\"", metrics.vertBearingX / SCALE_F, metrics.vertBearingY / SCALE_F, metrics.vertAdvance / SCALE_F);
  }
}

int outline_emitter_moveto(const FT_Vector*  to,
                           void*             user)

{
  if (!useSvgFontAndText) printf("   ");

  if (ceil(to->x / SCALE_F) == (to->x / SCALE_F) && ceil(to->y / SCALE_F) == to->y) {
    printf(" M %d %d", (int)ceil(to->x / SCALE_F), (int)ceil(to->y / SCALE_F));
  } else {
    printf(" M %f %f", to->x / SCALE_F, to->y / SCALE_F);
  }

  if (!useSvgFontAndText) printf("\n");

  return 0;
}

int outline_emitter_lineto(const FT_Vector*  to,
                           void*             user)
{
  if (!useSvgFontAndText) printf("   ");

  if (ceil(to->x / SCALE_F) == (to->x / SCALE_F) && ceil(to->y / SCALE_F) == to->y) {
    printf(" L %d %d", (int)ceil(to->x / SCALE_F), (int)ceil(to->y / SCALE_F));
  } else {
    printf(" L %f %f", to->x / SCALE_F, to->y / SCALE_F);
  }

  if (!useSvgFontAndText) printf("\n");

  return 0;
}

int outline_emitter_conicto(const FT_Vector*  control,
                            const FT_Vector*  to,
                            void*             user)
{
  if (!useSvgFontAndText) printf("   ");

  if (ceil(to->x / SCALE_F) == (to->x / SCALE_F) && ceil(to->y / SCALE_F) == to->y &&
      ceil(control->x / SCALE_F) == (control->x / SCALE_F) && ceil(control->y / SCALE_F) == (control->y / SCALE_F)) {
    printf(" Q %d %d %d %d", (int)ceil(control->x / SCALE_F), (int)ceil(control->y / SCALE_F), (int)ceil(to->x / SCALE_F), (int)ceil(to->y / SCALE_F));
  } else {
    printf(" Q %f %f %f %f", control->x / SCALE_F, control->y / SCALE_F, to->x / SCALE_F, to->y / SCALE_F);
  }

  if (!useSvgFontAndText) printf("\n");

  return 0;
}

int outline_emitter_cubicto(const FT_Vector*  control1,
                            const FT_Vector*  control2,
                            const FT_Vector*  to,
                            void*             user)
{
  if (!useSvgFontAndText) printf("   ");

  if (ceil(to->x / SCALE_F) == (to->x / SCALE_F) && ceil(to->y / SCALE_F) == to->y &&
      ceil(control1->x / SCALE_F) == (control1->x / SCALE_F) && ceil(control1->y / SCALE_F) == (control1->y / SCALE_F) &&
      ceil(control2->x / SCALE_F) == (control2->x / SCALE_F) && ceil(control2->y / SCALE_F) == (control2->y / SCALE_F)) {
    printf(" C %d %d %d %d %d %d", (int)ceil(control1->x / SCALE_F), (int)ceil(control1->y / SCALE_F), (int)ceil(control2->x / SCALE_F), (int)ceil(control2->y / SCALE_F), (int)ceil(to->x / SCALE_F), (int)ceil(to->y / SCALE_F));
  } else {
    printf(" C %f %f %f %f %f %f", control1->x / SCALE_F, control1->y / SCALE_F, control2->x / SCALE_F, control2->y / SCALE_F, to->x / SCALE_F, to->y / SCALE_F);
  }

  if (!useSvgFontAndText) printf("\n");

  return 0;
}

void printHelp(char* exeName)
{
  printf(" usage: %s fontfile <OPTION> string <OPTION2>\n", exeName);
  printf("  OPTION and string. OPTION2 is x(fill mode on)\n", exeName);
  printf(" [-g] GlyphName [x]\n");
  printf("  -c  CharCode(hex) [x]\n");
  printf("  -p  CharIndex [x]\n");
  printf(" ex.) %s ipamjm.ttf -g mj025760 x\n", exeName);
  printf("      %s ipamjm.ttf -c 0x8FBB\n", exeName);
  printf("      %s ipamjm.ttf -p 255\n", exeName);
  // printf("Copyright(C) 2012, Hiroshima University, suzuki toshiya <mpsuzuki@hiroshima-u.ac.jp>\n");
  return;
}

FT_ULong getCharCodeIntFromGlyphIndex(FT_Face   ft2face,
                                      FT_ULong  gid)
{
  FT_UInt   tmpGid;
  FT_ULong  charCode = FT_Get_First_Char(ft2face, &tmpGid);


  do {
   if (tmpGid == gid)
     return charCode;

   charCode = FT_Get_Next_Char(ft2face, charCode, &tmpGid);
  } while (tmpGid != 0);

  if (tmpGid == 0)
    return 0;
  else if (ft2face->charmap->encoding == FT_ENCODING_UNICODE)
    return 0xFFFD; // UCS missing character
  else
    return 0;
}

void procAGlyph(FT_Face  ft2face,
                FT_UInt  gid,
                char*    glyphName,
                FT_Bool  printHeader,     
                FT_Bool  printFooter)
{
  FT_Error  ft2err;
  FT_ULong  charCode; // for SvgFontAndText


  if (!scanAllGlyphs && gid == 0 && glyphName[0]) {
    printf("*** could not find glyph named %s in %s\n", glyphName, ft2face->family_name);
    return;
  }

  {
    FT_BBox           bbox;
    FT_Glyph_Metrics  vMetric;


    ft2err = FT_Load_Glyph(ft2face, gid, 0
                                         | FT_LOAD_NO_SCALE
                                         | FT_LOAD_VERTICAL_LAYOUT
                                         | (useHint > 0 ? 0 : FT_LOAD_NO_HINTING)
                                         | (useAutoHint > 0 ? FT_LOAD_FORCE_AUTOHINT : FT_LOAD_NO_AUTOHINT)
                          );
    if (ft2err != FT_Err_Ok) {
      printf("*** could not load glyph named %s (gid=%d) for vertical writing mode, err=%d\n", glyphName, gid, ft2err);
      return;
    }
    vMetric = ft2face->glyph->metrics;
    ft2err = FT_Load_Glyph(ft2face, gid, 0
                                         | FT_LOAD_NO_SCALE
                                         | (useHint > 0 ? 0 : FT_LOAD_NO_HINTING)
                                         | (useAutoHint > 0 ? FT_LOAD_FORCE_AUTOHINT : FT_LOAD_NO_AUTOHINT)
                          );
    if (ft2err != FT_Err_Ok) {
      printf("*** could not load glyph named %s (gid=%d), err=%d\n", glyphName, gid, ft2err);
      return;
    }

    ft2err = FT_Outline_Get_BBox(&(ft2face->glyph->outline), &bbox);
    if (ft2err != FT_Err_Ok) {
      printf("*** could not get bbox for glyph named %s (gid=%d), err=%d\n", glyphName, gid, ft2err);
      return;
    }
    if ((bbox.xMax > bbox.xMin || bbox.yMax > bbox.yMin) && gid < firstValidGid) {
      firstValidGid = gid;
    }

    if (printHeader) {
      if (!useSvgFontAndText) {
        printSvgHeader(&(ft2face->glyph->metrics));
      } else {
        printHtmlAndSvgHeader(NULL);
        printf("<defs>\n");
      }
      printf("  <font");
      printf(" id=\"%s%s%s\">\n", ft2face->family_name, useSvgFontAndText ? "" : "/", useSvgFontAndText ? "" : glyphName);
      printf("  <!-- fontBBox=\"%d %d %d %d\" -->\n", ft2face->bbox.xMin, ft2face->bbox.yMin, ft2face->bbox.xMax, ft2face->bbox.yMax);

      printf("    <font-face font-family=\"%s\" ", ft2face->family_name);
      printFaceMetrics(ft2face);
      printf("/>\n");
    }

    printf("      <!-- gid=\"%d\" glyphBBox=\"%d %d %d %d\" -->\n", gid, bbox.xMin, bbox.yMin, bbox.xMax, bbox.yMax);
    printf("      <glyph");
    if (useSvgFontAndText || addUcsInfo) {
      charCode = getCharCodeIntFromGlyphIndex(ft2face, gid);
      if ((charCode == 0 || charCode == 0xFFFD) && !ft2face->num_charmaps && scanAllGlyphs) { // charmap-less font like CFF in Type1-embedded PDF
        charCode = gid + 0xF000;
      }
      if (ft2face->charmap->encoding == FT_ENCODING_UNICODE) {
        printf(" unicode=\"U+%04X\"", charCode);
      } else
      if (ft2face->charmap->encoding == FT_ENCODING_SJIS) {
        printf(" sjis=\"0x%04X\"", charCode);
      } else
      if (ft2face->charmap->encoding == FT_ENCODING_GB2312) {
        printf(" gb2312=\"0x%04X;\"", charCode);
      } else
      if (ft2face->charmap->encoding == FT_ENCODING_BIG5) {
        printf(" big5=\"0x%04X\"", charCode);
      } else
      if (ft2face->charmap->encoding == FT_ENCODING_WANSUNG) {
        printf(" wansung=\"0x%04X\"", charCode);
      } else
      if (ft2face->charmap->encoding == FT_ENCODING_JOHAB) {
        printf(" johab=\"0x%04X\"", charCode);
      }
    } 
    printf(" int-gid=\"%d\"", gid);
    printf(" glyph-name=\"%s\" ", glyphName);
    printGlyphMetricHorizontal(ft2face->glyph->metrics);
    printf(" ");
    printGlyphMetricVertical(vMetric);

    if (printFooter && !useSvgFontAndText) {
      printf("/>\n");
      printf("  </font>\n");
      printf("<g>\n");
      printf("  <g transform=\"matrix(1, 0, 0, -1, %d, %d)\">\n",
              keepBearingX ? 0 : (- ft2face->glyph->metrics.horiBearingX),
              ft2face->glyph->metrics.horiBearingY
      );
      printf("  <path ");
    }

    {
      FT_Outline_Funcs  outline_emitters;

      outline_emitters.move_to = &outline_emitter_moveto;
      outline_emitters.line_to = &outline_emitter_lineto;
      outline_emitters.conic_to = &outline_emitter_conicto;
      outline_emitters.cubic_to = &outline_emitter_cubicto;
      outline_emitters.shift = 0;
      outline_emitters.delta = 0;

      printf(" d=\"");
      if (!useSvgFontAndText) {
        printf("\n");
      }
      ft2err = FT_Outline_Decompose(&(ft2face->glyph->outline), &outline_emitters, &useSvgFontAndText);

      if (ft2err != FT_Err_Ok) {
        printf("*** something goes wrong in outline deomposition\n");
        return;
      }
      if (!useSvgFontAndText) {
        printf("   ");
      }
      printf(" Z\"");
      if (doFill)
        printf(" stroke=\"black\" fill=\"black\"");
      printf("/>\n");
    }
  }

  if (!printFooter)
    return;
  else if (useSvgFontAndText) {
    printf("  </font>\n");
    printf("</defs>\n");
    printf("</svg>\n");
    printf("<div style=\"font-size: 48px; font-family: %s;\">", ft2face->family_name);
    printf("&#x%04X;", charCode);
    printf("</div>\n");
    printf("</body>\n");
    printf("</html>\n");
  } else {
    printf("  </g>\n");
    printf("</g>\n");
  }
}

int main(int argc, char** argv)
{
  FT_Error    ft2err;
  FT_Library  ft2lib;
  FT_Face     ft2face;
  FT_UInt     gid;
  int         opt;

  while (-1 != (opt = getopt(argc, argv, "ac:fg:hp:txCF:X"))) {
    switch (opt) {
    /* options take no arguments */
    case 'a': scanAllGlyphs = 1;     break;
    case 'f': useAutoHint = 1;       break;
    case 'h': useHint = 1;           break;
    case 't': useSvgFontAndText = 1; break;
    case 'x': doFill = 1;            break;
    case 'C': addUcsInfo = 1;        break;
    case 'X': keepBearingX = 1;      break;

    /* options take single argment */
    case 'c': codeToSetGID = strtoul(optarg, NULL, 16); break;
    case 'g': glyphNameToSetGID = optarg;               break; 
    case 'p': gid = strtod(optarg, NULL);               break;
    case 'F': faceIndex = strtod(optarg, NULL);         break;
    default:
      printHelp(argv[0]);
    }
  }

  ft2err = FT_Init_FreeType( &ft2lib );
  if (ft2err != FT_Err_Ok) {
    printf("*** Could not initialize FreeType2 library\n");
    return -1;
  }

  if (optind >= argc)
    printHelp(argv[0]);

  ft2err = FT_New_Face(ft2lib, argv[optind], faceIndex, &ft2face);
  if (ft2err != FT_Err_Ok) {
    printf("*** Could not open %s face #%d\n", argv[1], faceIndex);
    return -1;
  }

  {
    if (codeToSetGID) {
      char buff[256];
      gid = FT_Get_Char_Index(ft2face, codeToSetGID);
      ft2err = FT_Get_Glyph_Name(ft2face, gid, buff, sizeof(buff));
      if (ft2err != FT_Err_Ok)
        buff[0] = '\0';
      procAGlyph(ft2face, gid, buff, 1, 1);
    } else
    if (glyphNameToSetGID) {
      gid = FT_Get_Name_Index(ft2face, glyphNameToSetGID);
      procAGlyph(ft2face, gid, glyphNameToSetGID, 1, 1);
    } else
    if (scanAllGlyphs) {
      for (gid = 0; gid < ft2face->num_glyphs; gid ++) {
        char buff[256];
        if (FT_Err_Ok != FT_Get_Glyph_Name(ft2face, gid, buff, sizeof(buff)))
          buff[0] = '\0';
        procAGlyph(ft2face, gid, buff, (gid == 0), 0);
      }
      printf("  </font>\n");
      printf("</defs>\n");
      printf("</svg>\n");
      printf("<div style=\"font-size: 48px; font-family: %s;\">\n", ft2face->family_name);

      for (gid = firstValidGid; gid < ft2face->num_glyphs; gid++) {
        FT_ULong  charCode = getCharCodeIntFromGlyphIndex(ft2face, gid);

        if ((charCode == 0 || charCode == 0xFFFD) && !ft2face->num_charmaps && scanAllGlyphs) { // charmap-less font like CFF in Type1-embedded PDF
          charCode = gid + 0xF000;
        }
        printf("&#x%04X;\n", charCode);
      }
      printf("</div>\n");
      printf("</body>\n");
      printf("</html>\n");
    }
  }

  if (!useSvgFontAndText)
    printf("</svg>\n");

  ft2err = FT_Done_Face(ft2face);
  if (ft2err != FT_Err_Ok) {
    printf("*** something goes wrong in face freeing\n");
    return -1;
  }

  ft2err = FT_Done_FreeType(ft2lib);
  if (ft2err != FT_Err_Ok) {
    printf("*** something goes wrong in FreeType2 freeing\n");
    return -1;
  }

  return 0;
}
