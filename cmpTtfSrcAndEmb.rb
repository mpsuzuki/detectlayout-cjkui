#!/usr/bin/env ruby1.9
require "getOpts.rb"
require "utils.rb"

def fillHashFromFileH(fp, hs)
  STDERR.printf("parse %s ", fp)
  fh = File::open(fp, "r")

  cnt = 0
  while (fh.gets())
    $_ = $_.chomp()
    next if ($_ =~ /^#/)
    tmp_hs = Hash.new()
    $_.split(" ").each do |tok|
      k, v = tok.split("=", 2)
      tmp_hs[k] = v
    end

    glyf = Hash.new()
    [ "ucs", "name", "gid" ].each do |k|
      next if (!tmp_hs.include?(k))
      glyf[k] = tmp_hs[k]
    end

    [ "crc8", "crc16", "crc32", "crc64", "md5" ].each do |k|
      hv = [k, tmp_hs[k]].join("=")
      if (hs.include?(hv))
        hs[hv] << glyf
      else
        hs[hv] = [glyf]
      end
    end

    STDERR.printf("\b") if (0 < cnt)
    STDERR.printf(".") if (0 == (cnt % 1000))
    if (0 == cnt % 4)
      STDERR.printf("|")
    elsif (1 == cnt % 4)
      STDERR.printf("/")
    elsif (2 == cnt % 4)
      STDERR.printf("-")
    elsif (3 == cnt % 4)
      STDERR.printf("\\")
    end

    cnt += 1
  end
  STDERR.printf("ok\n")

  fh.close()
end


fp_src = nil
if (Opts.include?("src"))
  fp_src = Opts.src
else
  fp_src = Opts.args[0]
end

fp_emb = nil
if (Opts.include?("emb"))
  fp_emb = Opts.emb
else
  fp_emb = Opts.args[1]
end

ttf_src = Hash.new()
ttf_emb = Hash.new()
fillHashFromFileH(fp_src, ttf_src)
fillHashFromFileH(fp_emb, ttf_emb)

ttf_emb.keys().select{|k| k =~ /^crc64=/}.each do |hv|
# ttf_emb.keys().select{|k| k =~ /^crc32=/}.each do |hv|
# ttf_emb.keys().select{|k| k =~ /^crc16=/}.each do |hv|
  # p [ttf_emb[hv], ttf_src[hv]]
  printf("%s -> %s\n",
         ttf_emb[hv].sort_by{|hs| hs["gid"].to_i()}.collect{|hs|
           sprintf("%s:%05d", fp_emb.basename_without_suffix(), hs["gid"].to_i())
         }.join(","),
         ttf_src[hv] == nil ? "nil" : 
           ttf_src[hv].sort_by{|hs| hs["ucs"].length}.reverse().collect{|hs|
             sprintf("%s:%s:%s:%05d",
               fp_src.basename_without_suffix(), hs["ucs"], hs["name"], hs["gid"].to_i())
           }.join(", "))
end
