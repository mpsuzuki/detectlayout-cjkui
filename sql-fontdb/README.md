# utilities to access SQL-based font database

the database consists of 2 tables (at present)
* columns in font_file
    pathname	text primary key
    pathpdf	text
    basedump	text
    suffix	text (ttf, cff, or bin)
    family	text
    family_norm	text
    sfnt_name_family	text
    sfnt_name_ps	text
    revision	text
    created	text
    modified	text
    num_glyph	integer
    num_g2u	integer
    num_dup	integer
    num_ascii	integer
    num_cjk	integer
    sfnts	text
    cid_ros	text
* columns in pdf_file
    pathname	text primary key
    basename	text
    pdfversion	text
    created	text
    modified	text
    author	text
    title	text
    producer	text


