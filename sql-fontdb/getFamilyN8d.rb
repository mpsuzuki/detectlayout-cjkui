#!/usr/bin/env ruby1.9
Encoding.default_internal = "utf-8"
Encoding.default_external = "utf-8"
require "sqlite3"
require "rotator.rb"
require "utils.rb"
Opts = {
  "with-sfnt" => "",
  "without-sfnt" => "",
  "db" => "test.sqlite3"
}
require "getOpts.rb"

if (Opts.include?("with"))
  Opts["with-sfnt"] += ("," + Opts["with"])
end
Opts["with-sfnt"] = Opts["with-sfnt"].split(",").select{|v|
                      v && v.length > 0
                    }.compact()

if (Opts.include?("without"))
  Opts["without-sfnt"] += ("," + Opts["without"])
end
Opts["without-sfnt"] = Opts["without-sfnt"].split(",").select{|v|
                         v && v.length > 0
                       }.compact()

# if --without option includes preset value in with-sfnt, exclude it.
if (Opts["without-sfnt"].include?("glyf"))
  Opts["with-sfnt"] = Opts["with-sfnt"].select{|v| v != "glyf"}
end

sfnt_pat = ""
Opts["with-sfnt"].each do |tag|
  sfnt_pat += ("AND sfnts LIKE \"%," + tag + ",%\" ")
end
Opts["without-sfnt"].each do |tag|
  sfnt_pat += ("AND sfnts NOT LIKE \"%," + tag + ",%\" ")
end

rot = Rotator.new(5000)
sqldb = SQLite3::Database.open(Opts.db, {:readonly => "r"})

def removeCMapName(s)
  return s if (s == nil)
  s = s[7..-1] if (s =~ /^[A-Z]{6}\+/)
  toks = s.split("-")
  while (toks.length > 1)
    if ([ "H", "V",
          "RKSJ", "80pv", "90pv", "90ms",
          "EUC", "UHC", "KSCms", "KSCpc",
          "Identity", "WinCharSetFFFF" ].include?(toks.last))
      toks.pop()
    else
      break
    end
  end
  return toks.join("-")
end

# -----------------------------------------------------------------
rot.reset()
cmd0 = <<EOS
  SELECT COUNT(*), family_norm, family, pathpdf
    FROM font_file
    WHERE suffix = "ttf" AND sfnts LIKE "%,glyf,%"
    GROUP BY pathpdf
    ORDER BY family_norm, family
  ;
EOS

norm2family = Hash.new()
sqldb.execute(cmd0) do |arr|
  count, family_norm, family, sfnt_name_family, sfnt_name_ps, pathpdf, = arr
  family_norm = removeCMapName(family_norm)
  family = removeCMapName(family)
  sfnt_name_family = removeCMapName(sfnt_name_family)
  sfnt_name_ps = removeCMapName(sfnt_name_ps)
  norm2family[family_norm] = Hash.new() if (!norm2family.include?(family_norm))
  if (norm2family[family_norm].include?(family))
    norm2family[family_norm][family] += count
  else
    norm2family[family_norm][family] = count
  end
  STDERR.printf(rot.get())
end
STDERR.puts("")

# -----------------------------------------------------------------
norm2numpdf = Hash.new()
norm2family.each do |norm, hs|
  numpdf = hs.values().inject("+")
  norm2numpdf[norm]  = numpdf
end

norm2numpdf.keys().sort_by{|norm| norm2numpdf[norm]}.each do |norm|
  printf("%s\t%d\n", norm, norm2numpdf[norm])
  norm2family[norm].keys().sort_by{|family| norm2family[norm][family]}.each do |family|
    printf("\t%d\t%s\n", norm2family[norm][family], family)
  end
end
