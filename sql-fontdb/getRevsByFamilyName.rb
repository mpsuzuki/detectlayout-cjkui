#!/usr/bin/env ruby1.9
Encoding.default_internal = "utf-8"
Encoding.default_external = "utf-8"
require "sqlite3"
require "rotator.rb"
require "utils.rb"
Opts = {
  "db" => "test.sqlite3",
  "with-sfnt" => "glyf",
  "without-sfnt" => "",
  "family" => "%MSGothic%"
}
require "getOpts.rb"

if (Opts.include?("with"))
  Opts["with-sfnt"] += ("," + Opts["with"])
end
Opts["with-sfnt"] = Opts["with-sfnt"].split(",").select{|v|
                      v && v.length > 0
                    }.compact()

if (Opts.include?("without"))
  Opts["without-sfnt"] += ("," + Opts["without"])
end
Opts["without-sfnt"] = Opts["without-sfnt"].split(",").select{|v|
                         v && v.length > 0
                       }.compact()

# if --without option includes preset value in with-sfnt, exclude it.
if (Opts["without-sfnt"].include?("glyf"))
  Opts["with-sfnt"] = Opts["with-sfnt"].select{|v| v != "glyf"}
end

sfnt_pat = ""
Opts["with-sfnt"].each do |tag|
  sfnt_pat += ("AND sfnts LIKE \"%," + tag + ",%\" ")
end
Opts["without-sfnt"].each do |tag|
  sfnt_pat += ("AND sfnts NOT LIKE \"%," + tag + ",%\" ")
end

rot = Rotator.new(5000)
sqldb = SQLite3::Database.open(Opts.db, {:readonly => "r"})

def removeCMapName(s)
  return s if (s == nil)
  s = s[7..-1] if (s =~ /^[A-Z]{6}\+/)
  toks = s.split("-")
  while (toks.length > 1)
    if ([ "H", "V",
          "RKSJ", "80pv", "90pv", "90ms",
          "EUC", "UHC", "KSCms", "KSCpc",
          "Identity", "WinCharSetFFFF" ].include?(toks.last))
      toks.pop()
    else
      break
    end
  end
  return toks.join("-")
end


class RevInfo
  attr_accessor(:revision, :family_name,
                :modif_font, :creat_pdf,
                :count_instance, :max_num_glyph, :max_num_g2u, :pathname_max_glyph,
                :psname, :producer)
  def initialize(family_name, rev, modif_font)
    @family_name = family_name
    @revision = rev
    @modif_font = modif_font
    @count_instance = 0
    @max_num_glyph = -1
    @max_num_g2u = -1
    @psname = Array.new()
    @producer = Array.new()
  end
  def update(num_glyph, num_g2u, pathfont, creat_pdf, psname, producer)
    psname = removeCMapName(psname)
    if (!@psname.include?(psname))
      @psname << psname
    end
    if (!@producer.include?(producer))
      @producer << producer
    end

    if (@creat_pdf && creat_pdf)
      @creat_pdf.unshift(creat_pdf) if (!@creat_pdf.first || creat_pdf < @creat_pdf.first)
      @creat_pdf.push(creat_pdf) if (!@creat_pdf.last  || creat_pdf > @creat_pdf.last)
    elsif (creat_pdf)
      @creat_pdf = [creat_pdf]
    end
    if (@creat_pdf.length > 1)
      @creat_pdf.shift() if (@creat_pdf[0] == "...._.._.._..:..:.._UTC")
    end

    @count_instance += 1
    if (num_glyph && num_glyph > @max_num_glyph)
      @max_num_glyph = num_glyph
      @max_num_g2u = num_g2u if (num_g2u.to_i() < 65536)
      @pathname_max_glyph = pathfont
    end
  end
  def dump()
    # p [ @revision, @count_instance, @max_num_glyphm, @max_num_g2u, @pathname_max_glyph, @pathname_max_g2u]
    puts [
           # @psname.join(","),
           @revision,
           @count_instance.to_s(),
           @max_num_glyph.to_s(),
           @max_num_g2u ? @max_num_g2u.to_s() : "(nil)",
           @modif_font ? @modif_font.gsub("x", ".") : "(nil)",
           [@creat_pdf.first, @creat_pdf.last].map{|v|
             v.split("_")[0..-3].join("_")
           }.join(".."),
           @pathname_max_glyph
    ] .join("\t")

    if Opts.include?("show-psname")
      puts "\t" + @psname.join(" ") if (@psname.length > 1)
    end
    if Opts.include?("show-producer")
      puts "\t" + @producer.join(" ") if (@producer.length > 1)
    end
  end
end


# -----------------------------------------------------------------
rot.reset()
cmd0 = <<EOS
  SELECT font_file.family_norm, font_file.revision,
         font_file.created, font_file.modified,
         font_file.sfnt_name_ps,
         font_file.num_glyph, font_file.num_g2u,
         pdf_file.created, pdf_file.modified,
         font_file.pathname, pdf_file.pathname, pdf_file.producer
    FROM font_file JOIN pdf_file ON font_file.pathpdf = pdf_file.pathname
    WHERE font_file.suffix = "ttf"
      AND font_file.num_glyph IS NOT NULL
      __sfnt_pat__
      AND font_file.family_norm LIKE __family_norm__
  ;
EOS

rev2info = Hash.new()
cmd0 = cmd0.gsub("__family_norm__", "\"" + Opts.family + "\"")
           .gsub("__sfnt_pat__", sfnt_pat)
sqldb.execute(cmd0){|arr|
  family_norm, rev, creat_font, modif_font, sfnt_name_ps,
    num_glyph, num_g2u, creat_pdf, modif_pdf,
    pathfont, pathpdf, producer, = arr

  if (!modif_font)
    modif_font = "xxxx_xx_xx"
  end

  if (!creat_pdf)
    creat_pdf = "...._.._.._..:..:.._UTC"
  end

  if (!rev2info.include?(rev))
    rev2info[rev] = Hash.new()
  end
  if (!rev2info[rev].include?(modif_font))
    rev2info[rev][modif_font] = RevInfo.new(family_norm, rev, modif_font)
  end
  rev2info[rev][modif_font].update(num_glyph, num_g2u, pathpdf, creat_pdf, sfnt_name_ps, producer)
  STDERR.printf(rot.get())
}
STDERR.puts("")

# -----------------------------------------------------------------
lastRev = nil
rev2info.keys().sort().each_with_index do |rev, i|
  puts("") if (i > 0 && lastRev != rev)
  rev2info[rev].keys().sort().each do |modif_font|
    rev2info[rev][modif_font].dump()
  end
  lastRev = rev
end
